xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://pruebabideafactory.com/VerificacionRutNormalizado";
(:: import schema at "../Schemas/VerificacionRutNormalizado.xsd" ::)

declare variable $string as xs:string external;
declare variable $string1 as xs:string external;

declare function local:func($string as xs:string, 
                            $string1 as xs:string) 
                            as element() (:: schema-element(ns1:VerificarRutResponse) ::) {
    <ns1:VerificarRutResponse/>
};

local:func($string, $string1)
