xquery version "1.0" encoding "utf-8";
(:: OracleAnnotationVersion "1.0" ::)
declare namespace ns1="http://pruebabideafactory.com/VerificacionRutNormalizado";
(:: import schema at "../Schemas/VerificacionRutNormalizado.xsd" ::)
declare variable $VerificarRut as element() (:: schema-element(ns1:VerificarRut) ::) external;

declare function local:VerificacionDV($VerificarRut as element() (:: schema-element(ns1:VerificarRut) ::)) as element(*) {
    let $numeroRUT := fn:replace(fn:substring-before(fn:data($VerificarRut/BodyEntradaVerificarRut/numeroRut),'-'),"\.","")
    let $DV := fn:substring-after(fn:data($VerificarRut/BodyEntradaVerificarRut/numeroRut),'-')
    let $num := fn:string-length($numeroRUT)
    let $digito1:=xs:integer(fn:substring($numeroRUT,$num,1))
    let $digito2:=xs:integer(fn:substring($numeroRUT,$num -1,1))
    let $digito3:=xs:integer(fn:substring($numeroRUT,$num -2,1))
    let $digito4:=xs:integer(fn:substring($numeroRUT,$num -3,1))
    let $digito5:=xs:integer(fn:substring($numeroRUT,$num -4,1))
    let $digito6:=xs:integer(fn:substring($numeroRUT,$num -5,1))
    let $digito7:=xs:integer(fn:substring($numeroRUT,$num -6,1))
    let $digito8:=xs:integer(fn:substring($numeroRUT,$num -7,1))
    return
    <VERIFICACION>
    {
        if (fn:not(fn:empty($digito1))) then
            if (fn:not(fn:empty($digito2))) then
                if (fn:not(fn:empty($digito3))) then
                    if (fn:not(fn:empty($digito4))) then
                        if (fn:not(fn:empty($digito5))) then
                            if (fn:not(fn:empty($digito6))) then
                                if (fn:not(fn:empty($digito7))) then
                                    if (fn:not(fn:empty($digito8))) then
                                            <respuesta>
                                                {
                                                    let $DigitoVer:=11-((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6)+($digito6*7)+($digito7*2)+($digito8*3))-(11*(xs:integer((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6)+($digito6*7)+($digito7*2)+($digito8*3))div 11))))
                                                    return
                                                    <res>
                                                       { if ($DigitoVer = 10) then
                                                            let $valorDigito := "K"
                                                            return
                                                                <r>
                                                                {
                                                                    if ($valorDigito = $DV) then
                                                                        <a>
                                                                            <codigoVerificacion>0000</codigoVerificacion>
                                                                            <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                        </a>
                                                                    else(
                                                                            <a>
                                                                                <codigoVerificacion>9999</codigoVerificacion>
                                                                                <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                            </a>
                                                                        )
                                                                }
                                                                </r>
                                                          else  
                                                            if ($DigitoVer = 11) then
                                                                let $valorDigito := "0"
                                                                return
                                                                     <r>
                                                                    {
                                                                        if ($valorDigito = $DV) then
                                                                            <a>
                                                                                <codigoVerificacion>0000</codigoVerificacion>
                                                                                <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                            </a>
                                                                        else(
                                                                                <a>
                                                                                    <codigoVerificacion>9999</codigoVerificacion>
                                                                                    <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                                </a>
                                                                            )
                                                                    }
                                                                    </r>
                                                            else
                                                                let $valorDigito := xs:string($DigitoVer)
                                                                return
                                                                     <r>
                                                                {
                                                                    if ($valorDigito = $DV) then
                                                                        <a>
                                                                            <codigoVerificacion>0000</codigoVerificacion>
                                                                            <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                        </a>
                                                                    else(
                                                                            <a>
                                                                                <codigoVerificacion>9999</codigoVerificacion>
                                                                                <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                            </a>
                                                                        )
                                                                }
                                                                </r>
                                                    }
                                                    </res>
                                                }
                                                </respuesta>
                                    else(
                                            <respuesta>
                                            {
                                                let $DigitoVer:=11-((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6)+($digito6*7)+($digito7*2))-(11*(xs:integer((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6)+($digito6*7)+($digito7*2)) div 11))))
                                                return
                                                <res>
                                                   { if ($DigitoVer = 10) then
                                                        let $valorDigito := "K"
                                                        return
                                                            <r>
                                                            {
                                                                if ($valorDigito = $DV) then
                                                                    <a>
                                                                        <codigoVerificacion>0000</codigoVerificacion>
                                                                        <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                    </a>
                                                                else(
                                                                        <a>
                                                                            <codigoVerificacion>9999</codigoVerificacion>
                                                                            <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                        </a>
                                                                    )
                                                            }
                                                            </r>
                                                      else  
                                                        if ($DigitoVer = 11) then
                                                            let $valorDigito := "0"
                                                            return
                                                                 <r>
                                                                {
                                                                    if ($valorDigito = $DV) then
                                                                        <a>
                                                                            <codigoVerificacion>0000</codigoVerificacion>
                                                                            <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                        </a>
                                                                    else(
                                                                            <a>
                                                                                <codigoVerificacion>9999</codigoVerificacion>
                                                                                <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                            </a>
                                                                        )
                                                                }
                                                                </r>
                                                        else
                                                            let $valorDigito := xs:string($DigitoVer)
                                                            return
                                                                 <r>
                                                            {
                                                                if ($valorDigito = $DV) then
                                                                    <a>
                                                                        <codigoVerificacion>0000</codigoVerificacion>
                                                                        <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                    </a>
                                                                else(
                                                                        <a>
                                                                            <codigoVerificacion>9999</codigoVerificacion>
                                                                            <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                        </a>
                                                                    )
                                                            }
                                                            </r>
                                                }
                                                </res>
                                            }
                                            </respuesta>
                                        )
                                else(
                                        <respuesta>
                                        {
                                            let $DigitoVer:=11-((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6)+($digito6*7))-(11*(xs:integer((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6)+($digito6*7)) div 11))))
                                            return
                                            <res>
                                               { if ($DigitoVer = 10) then
                                                    let $valorDigito := "K"
                                                    return
                                                        <r>
                                                        {
                                                            if ($valorDigito = $DV) then
                                                                <a>
                                                                    <codigoVerificacion>0000</codigoVerificacion>
                                                                    <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                </a>
                                                            else(
                                                                    <a>
                                                                        <codigoVerificacion>9999</codigoVerificacion>
                                                                        <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                    </a>
                                                                )
                                                        }
                                                        </r>
                                                  else  
                                                    if ($DigitoVer = 11) then
                                                        let $valorDigito := "0"
                                                        return
                                                             <r>
                                                            {
                                                                if ($valorDigito = $DV) then
                                                                    <a>
                                                                        <codigoVerificacion>0000</codigoVerificacion>
                                                                        <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                    </a>
                                                                else(
                                                                        <a>
                                                                            <codigoVerificacion>9999</codigoVerificacion>
                                                                            <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                        </a>
                                                                    )
                                                            }
                                                            </r>
                                                    else
                                                        let $valorDigito := xs:string($DigitoVer)
                                                        return
                                                             <r>
                                                        {
                                                            if ($valorDigito = $DV) then
                                                                <a>
                                                                    <codigoVerificacion>0000</codigoVerificacion>
                                                                    <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                </a>
                                                            else(
                                                                    <a>
                                                                        <codigoVerificacion>9999</codigoVerificacion>
                                                                        <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                    </a>
                                                                )
                                                        }
                                                        </r>
                                            }
                                            </res>
                                        }
                                        </respuesta>
                                    )
                            else(
                                    <respuesta>
                                    {
                                        let $DigitoVer:=11-((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6))-(11*(xs:integer((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)+($digito5*6)) div 11))))
                                        return
                                        <res>
                                           { if ($DigitoVer = 10) then
                                                let $valorDigito := "K"
                                                return
                                                    <r>
                                                    {
                                                        if ($valorDigito = $DV) then
                                                            <a>
                                                                <codigoVerificacion>0000</codigoVerificacion>
                                                                <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                            </a>
                                                        else(
                                                                <a>
                                                                    <codigoVerificacion>9999</codigoVerificacion>
                                                                    <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                </a>
                                                            )
                                                    }
                                                    </r>
                                              else  
                                                if ($DigitoVer = 11) then
                                                    let $valorDigito := "0"
                                                    return
                                                         <r>
                                                        {
                                                            if ($valorDigito = $DV) then
                                                                <a>
                                                                    <codigoVerificacion>0000</codigoVerificacion>
                                                                    <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                                </a>
                                                            else(
                                                                    <a>
                                                                        <codigoVerificacion>9999</codigoVerificacion>
                                                                        <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                    </a>
                                                                )
                                                        }
                                                        </r>
                                                else
                                                    let $valorDigito := xs:string($DigitoVer)
                                                    return
                                                         <r>
                                                    {
                                                        if ($valorDigito = $DV) then
                                                            <a>
                                                                <codigoVerificacion>0000</codigoVerificacion>
                                                                <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                            </a>
                                                        else(
                                                                <a>
                                                                    <codigoVerificacion>9999</codigoVerificacion>
                                                                    <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                </a>
                                                            )
                                                    }
                                                    </r>
                                        }
                                        </res>
                                    }
                                    </respuesta>
                                )
                        else(
                                <respuesta>
                                {
                                    let $DigitoVer:=11-((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5))-(11*(xs:integer((($digito1*2)+($digito2*3)+($digito3*4)+($digito4*5)) div 11))))
                                    return
                                    <res>
                                       { if ($DigitoVer = 10) then
                                            let $valorDigito := "K"
                                            return
                                                <r>
                                                {
                                                    if ($valorDigito = $DV) then
                                                        <a>
                                                            <codigoVerificacion>0000</codigoVerificacion>
                                                            <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                        </a>
                                                    else(
                                                            <a>
                                                                <codigoVerificacion>9999</codigoVerificacion>
                                                                <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                            </a>
                                                        )
                                                }
                                                </r>
                                          else  
                                            if ($DigitoVer = 11) then
                                                let $valorDigito := "0"
                                                return
                                                     <r>
                                                    {
                                                        if ($valorDigito = $DV) then
                                                            <a>
                                                                <codigoVerificacion>0000</codigoVerificacion>
                                                                <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                            </a>
                                                        else(
                                                                <a>
                                                                    <codigoVerificacion>9999</codigoVerificacion>
                                                                    <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                                </a>
                                                            )
                                                    }
                                                    </r>
                                            else
                                                let $valorDigito := xs:string($DigitoVer)
                                                return
                                                     <r>
                                                {
                                                    if ($valorDigito = $DV) then
                                                        <a>
                                                            <codigoVerificacion>0000</codigoVerificacion>
                                                            <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                        </a>
                                                    else(
                                                            <a>
                                                                <codigoVerificacion>9999</codigoVerificacion>
                                                                <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                            </a>
                                                        )
                                                }
                                                </r>
                                    }
                                    </res>
                                }
                                </respuesta>
                            )
                    else(
                            <respuesta>
                            {
                                let $DigitoVer:=11-((($digito1*2)+($digito2*3)+($digito3*4))-(11*(xs:integer((($digito1*2)+($digito2*3)+($digito3*4)) div 11))))
                                return
                                <res>
                                   { if ($DigitoVer = 10) then
                                        let $valorDigito := "K"
                                        return
                                            <r>
                                            {
                                                if ($valorDigito = $DV) then
                                                    <a>
                                                        <codigoVerificacion>0000</codigoVerificacion>
                                                        <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                    </a>
                                                else(
                                                        <a>
                                                            <codigoVerificacion>9999</codigoVerificacion>
                                                            <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                        </a>
                                                    )
                                            }
                                            </r>
                                      else  
                                        if ($DigitoVer = 11) then
                                            let $valorDigito := "0"
                                            return
                                                 <r>
                                                {
                                                    if ($valorDigito = $DV) then
                                                        <a>
                                                            <codigoVerificacion>0000</codigoVerificacion>
                                                            <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                        </a>
                                                    else(
                                                            <a>
                                                                <codigoVerificacion>9999</codigoVerificacion>
                                                                <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                            </a>
                                                        )
                                                }
                                                </r>
                                        else
                                            let $valorDigito := xs:string($DigitoVer)
                                            return
                                                 <r>
                                            {
                                                if ($valorDigito = $DV) then
                                                    <a>
                                                        <codigoVerificacion>0000</codigoVerificacion>
                                                        <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                    </a>
                                                else(
                                                        <a>
                                                            <codigoVerificacion>9999</codigoVerificacion>
                                                            <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                        </a>
                                                    )
                                            }
                                            </r>
                                }
                                </res>
                            }
                            </respuesta>
                        )
                else(
                        <respuesta>
                        {
                            let $DigitoVer:=11-((($digito1*2))-(11*(xs:integer((($digito1*2)) div 11))))
                            return
                            <res>
                               { if ($DigitoVer = 10) then
                                    let $valorDigito := "K"
                                    return
                                        <r>
                                        {
                                            if ($valorDigito = $DV) then
                                                <a>
                                                    <codigoVerificacion>0000</codigoVerificacion>
                                                    <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                </a>
                                            else(
                                                    <a>
                                                        <codigoVerificacion>9999</codigoVerificacion>
                                                        <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                    </a>
                                                )
                                        }
                                        </r>
                                  else  
                                    if ($DigitoVer = 11) then
                                        let $valorDigito := "0"
                                        return
                                             <r>
                                            {
                                                if ($valorDigito = $DV) then
                                                    <a>
                                                        <codigoVerificacion>0000</codigoVerificacion>
                                                        <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                    </a>
                                                else(
                                                        <a>
                                                            <codigoVerificacion>9999</codigoVerificacion>
                                                            <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                        </a>
                                                    )
                                            }
                                            </r>
                                    else
                                        let $valorDigito := xs:string($DigitoVer)
                                        return
                                             <r>
                                        {
                                            if ($valorDigito = $DV) then
                                                <a>
                                                    <codigoVerificacion>0000</codigoVerificacion>
                                                    <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                </a>
                                            else(
                                                    <a>
                                                        <codigoVerificacion>9999</codigoVerificacion>
                                                        <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                    </a>
                                                )
                                        }
                                        </r>
                            }
                            </res>
                        }
                        </respuesta>
                    )
            else(
                    <respuesta>
                    {
                        let $DigitoVer:=11-((($digito1*2))-(11*(xs:integer((($digito1*2)) mod 11))))
                        return
                        <res>
                           { if ($DigitoVer = 10) then
                                let $valorDigito := "K"
                                return
                                    <r>
                                    {
                                        if ($valorDigito = $DV) then
                                            <a>
                                                <codigoVerificacion>0000</codigoVerificacion>
                                                <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                            </a>
                                        else(
                                                <a>
                                                    <codigoVerificacion>9999</codigoVerificacion>
                                                    <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                </a>
                                            )
                                    }
                                    </r>
                              else  
                                if ($DigitoVer = 11) then
                                    let $valorDigito := "0"
                                    return
                                         <r>
                                        {
                                            if ($valorDigito = $DV) then
                                                <a>
                                                    <codigoVerificacion>0000</codigoVerificacion>
                                                    <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                                </a>
                                            else(
                                                    <a>
                                                        <codigoVerificacion>9999</codigoVerificacion>
                                                        <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                    </a>
                                                )
                                        }
                                        </r>
                                else
                                    let $valorDigito := xs:string($DigitoVer)
                                    return
                                         <r>
                                    {
                                        if ($valorDigito = $DV) then
                                            <a>
                                                <codigoVerificacion>0000</codigoVerificacion>
                                                <mensajeRespuesta>Digito verificacion Valido</mensajeRespuesta>
                                            </a>
                                        else(
                                                <a>
                                                    <codigoVerificacion>9999</codigoVerificacion>
                                                    <mensajeRespuesta>Digito verificacion invalido</mensajeRespuesta>
                                                </a>
                                            )
                                    }
                                    </r>
                        }
                        </res>
                    } 
                    </respuesta>
                )
        else(
                <respuesta>
                    <res>
                        <r>
                            <a>
                                <codigoVerificacion>9999</codigoVerificacion>
                                <mensajeRespuesta>numero rut invalido</mensajeRespuesta>
                            </a>
                        </r>
                    </res>
                </respuesta>
            ) 
    }
    </VERIFICACION>  
};

local:VerificacionDV($VerificarRut)
