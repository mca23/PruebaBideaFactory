xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://TargetNamespace.com/ServiceName";
(:: import schema at "../Schemas/RS_JSON.xsd" ::)
declare namespace ns2="http://pruebabideafactory.com/VerificacionRutNormalizado";
(:: import schema at "../Schemas/VerificacionRutNormalizado.xsd" ::)

declare variable $Root-Element as element() (:: schema-element(ns1:Root-Element) ::) external;

declare function local:XMLToNormalizado($Root-Element as element() (:: schema-element(ns1:Root-Element) ::)) as element() (:: schema-element(ns2:VerificarRutResponse) ::) {
    <ns2:VerificarRutResponse>
        <HeaderSalida>
            <tipoMensaje>INFO</tipoMensaje>
            <mensajeUsuario>Transaccion Exitosa</mensajeUsuario>
            <codigoMensaje>0000</codigoMensaje>
            <fechaSalida>{ fn-bea:dateTime-to-string-with-format("yyyyMMdd", fn:adjust-dateTime-to-timezone(fn:current-dateTime(), ())) }</fechaSalida>
            <horaSalida>{ fn-bea:dateTime-to-string-with-format("HHmmss", fn:adjust-dateTime-to-timezone(fn:current-dateTime(), ())) }</horaSalida>
        </HeaderSalida>
        <BodySalidaVerificarRut>
            <numeroRut>{fn:data($Root-Element/ns1:rut)}</numeroRut>
            <nombre>{fn:data($Root-Element/ns1:nombre)}</nombre>
        </BodySalidaVerificarRut>
    </ns2:VerificarRutResponse>
};

local:XMLToNormalizado($Root-Element)
