xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://TargetNamespace.com/ServiceName";
(:: import schema at "../Schemas/RQ_JSON.xsd" ::)
declare namespace ns1="http://pruebabideafactory.com/VerificacionRutNormalizado";
(:: import schema at "../Schemas/VerificacionRutNormalizado.xsd" ::)

declare variable $VerificarRut as element() (:: schema-element(ns1:VerificarRut) ::) external;

declare function local:NormalizadoToXML($VerificarRut as element() (:: schema-element(ns1:VerificarRut) ::)) as element() (:: schema-element(ns2:Root-Element) ::) {
    <ns2:Root-Element>
        <ns2:rut>{fn:data($VerificarRut/BodyEntradaVerificarRut/numeroRut)}</ns2:rut>
    </ns2:Root-Element>
};

local:NormalizadoToXML($VerificarRut)
