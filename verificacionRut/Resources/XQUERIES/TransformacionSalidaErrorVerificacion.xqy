xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://pruebabideafactory.com/VerificacionRutNormalizado";
(:: import schema at "../Schemas/VerificacionRutNormalizado.xsd" ::)

declare variable $codigoVerificacion as xs:string external;
declare variable $mensajeRespuesta as xs:string external;


declare function local:TransformacionSalidaErrorVerificacion($codigoVerificacion as xs:string,
                                                             $mensajeRespuesta as xs:string) as element() (:: schema-element(ns1:VerificarRutResponse) ::) {
    <ns1:VerificarRutResponse>
        <HeaderSalida>
            <tipoMensaje>ERROR</tipoMensaje>
            <mensajeUsuario>{$mensajeRespuesta}</mensajeUsuario>
            <codigoMensaje>{$codigoVerificacion}</codigoMensaje>
            <fechaSalida>{ fn-bea:dateTime-to-string-with-format("yyyyMMdd", fn:adjust-dateTime-to-timezone(fn:current-dateTime(), ())) }</fechaSalida>
            <horaSalida>{ fn-bea:dateTime-to-string-with-format("HHmmss", fn:adjust-dateTime-to-timezone(fn:current-dateTime(), ())) }</horaSalida>
        </HeaderSalida>
    </ns1:VerificarRutResponse>
};

local:TransformacionSalidaErrorVerificacion($codigoVerificacion, $mensajeRespuesta)
